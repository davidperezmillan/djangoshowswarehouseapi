# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from rest_framework import viewsets
from djangoshowswarehouseapi.serializers import ShowSerializer, ScrapSerializer
from djangoshowswarehouseapi.models import Shows, Scraps

# Create your views here.

class ShowViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Shows.objects.all()
    serializer_class = ShowSerializer


class ScrapViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Scraps.objects.all()
    serializer_class = ScrapSerializer
