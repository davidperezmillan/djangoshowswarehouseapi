# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework import serializers

from djangoshowswarehouseapi.models import Shows, Scraps


class ShowSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shows
        fields = ('__all__')
        
        
class ScrapSerializer(serializers.ModelSerializer):
    class Meta:
        model = Scraps
        fields = ('__all__')
                