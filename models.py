# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Import sub models
# from .models.models import *


# Create your models here.
class Scraps(models.Model):
    id = models.AutoField(primary_key=True)
    url =  models.CharField(max_length=255)
    idscrap = models.IntegerField(blank=True, null=True)  

    def __repr__(self):
        return self.str()

    def __str__(self):
        return "{} : {}".format(self.idscrap, self.url)

    class Meta:
        verbose_name_plural = "Scraps"
        managed = True
        unique_together = (('url','idscrap'))


class Shows(models.Model):
    id = models.AutoField(primary_key=True)  # AutoField?
    title = models.CharField(max_length=255, unique=True) # This field type is a guess.
    idscrap = models.ManyToManyField(Scraps)
    
    def __repr__(self):
        return self.str()

    def __str__(self):
        return "{}".format(self.title)

    class Meta:
        verbose_name_plural = "Shows"
        managed = True
        unique_together = (('title',))
        
